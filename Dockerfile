# Pull base image.
FROM java:8-jre
MAINTAINER Renato Silva <resilva87@outlook.com>

# Install ElasticSearch.
RUN \
  cd /tmp && \
  wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.4.4.tar.gz --no-check-certificate && \
  tar xvzf elasticsearch-1.4.4.tar.gz && \
  rm -f elasticsearch-1.4.4.tar.gz && \
  mv /tmp/elasticsearch-1.4.4 /elasticsearch

# Install Logstash
RUN \
  cd /tmp && \
  wget https://download.elasticsearch.org/logstash/logstash/logstash-1.4.2.tar.gz --no-check-certificate && \
  tar xvzf logstash-1.4.2.tar.gz && \
  rm -f logstash-1.4.2.tar.gz && \
  mv /tmp/logstash-1.4.2 /logstash

# Copy logstash config.
ADD config/logstash/logstash.conf /etc/config/logstash/logstash.conf

# Install ElasticSearch-kopf plugin (http://logstash.net/docs/1.4.2/tutorials/getting-started-with-logstash) 
RUN /elasticsearch/bin/plugin -install lmenezes/elasticsearch-kopf

# Install Kibana.
RUN \
  cd /tmp && \
  wget https://download.elasticsearch.org/kibana/kibana/kibana-4.0.0-linux-x64.tar.gz --no-check-certificate && \
  tar xvzf kibana-4.0.0-linux-x64.tar.gz && \
  rm -f kibana-4.0.0-linux-x64.tar.gz && \
  mv kibana-4.0.0-linux-x64 /kibana

# Install supervisord.
RUN \
  apt-get update && \
  apt-get install -y --no-install-recommends supervisor

# Copy supervisor config.
ADD config/supervisor/supervisord.conf /etc/supervisor/supervisord.conf

# Define mountable directories.
VOLUME ["/data", "/var/log"]

# Define working directory.
WORKDIR /

# Set default command to supervisor.
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]

# Expose Elasticsearch ports.
#   - 9200: HTTP
#   - 9300: transport
EXPOSE 9200
EXPOSE 9300

# Expose Logstash
EXPOSE 5959

# Expose Kibana ports
EXPOSE 5601
