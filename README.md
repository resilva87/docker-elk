# docker-elk
Elasticsearch/Logstash/Kibana docker stack (heavily based and copied from [here](https://registry.hub.docker.com/u/hbussell/docker-fluentd-kibana/)). 

## DockerHub
https://hub.docker.com/r/resilva87/docker-elk/

## Motivation
You can use this Docker image to unify and centralize logging from your apps/servers/whatever with [Logstash](http://logstash.net/), [Elasticsearch](http://www.elasticsearch.org/) and [Kibana](http://www.elasticsearch.org/overview/kibana?camp=homepage) will provide a way to extract useful insights from your logs (checkout [this great demo](http://demo.packetbeat.com/#/dashboard/elasticsearch/Packetbeat%2520Statistics) for the full power of this stack).

## Using
`sudo docker run -p 9200:9200 -p 9300:9300 -p 5959:5959 -p 5601:5601 -v $YOUR_DATA_DIR:/data  --name elk_inst -i -t resilva87/docker-elk`

Exposed ports:

+ 9200 => elasticsearch (http access)
+ 9300 => elasticsearch (transport)
+ 5959 => logstash (send TCP data)
+ 5601 => kibana (http access)

Mounted volumes:

+ /data => elasticsearch data
+ /var/log => logs from all apps